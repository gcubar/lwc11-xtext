package org.eclipse.xtext.example.domainmodel.m2m

import org.eclipse.xtext.example.domainmodel.domainmodel.*
import org.eclipse.xtext.example.domainmodel.dbmodel.*
import java.util.*
import com.google.inject.Inject

class Domainmodel2Dbmodel {
  def create target : DbmodelFactory::eINSTANCE.createDatabase transform (List<Entity> entities) {
    for (entity : entities) {
      target.tables += transformEntity2Table(entity)
    }
  }

  def create target : DbmodelFactory::eINSTANCE.createTable transformEntity2Table (Entity source) {
    target.name = source.name
    val idColumn = DbmodelFactory::eINSTANCE.createColumn
    idColumn.name = "ID"
    idColumn.type = "INTEGER"
    target.columns += idColumn
    target.columns += source.features.filter(typeof(Property)).map(x|x.transformProperty2Column);
  }

  def create target : DbmodelFactory::eINSTANCE.createColumn transformProperty2Column (Property source) {
    target.name = source.name
    target.type = getColumnType(source)
  }

  def getColumnType (Property source) {
    switch (source.type.simpleName) {
      case "Boolean" : "BIT"
      case "String" : "VARCHAR"
      case "BigDecimal" : "DECIMAL"
      case "Date" : "DATE"
      default: "INTEGER" // type references
    }
  }
}
