package org.eclipse.xtext.example.domainmodel.m2m;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.mwe.core.WorkflowContext;
import org.eclipse.emf.mwe.core.issues.Issues;
import org.eclipse.emf.mwe.core.lib.AbstractWorkflowComponent2;
import org.eclipse.emf.mwe.core.monitor.ProgressMonitor;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.example.domainmodel.dbmodel.Database;
import org.eclipse.xtext.example.domainmodel.domainmodel.Entity;
import org.eclipse.xtext.xbase.resource.XbaseResource;

import com.google.common.collect.Iterables;
import com.google.inject.internal.Lists;

@SuppressWarnings("restriction")
public class Domainmodel2DbmodelWfComponent extends AbstractWorkflowComponent2 {
  private String modelSlot;
  private String outputSlot;

  public void setModelSlot(String modelSlot) {
    this.modelSlot = modelSlot;
  }

  public void setOutputSlot(String outputSlot) {
    this.outputSlot = outputSlot;
  }

  @Override
  protected void checkConfigurationInternal(Issues issues) {
    checkRequiredConfigProperty("modelSlot", modelSlot, issues);
    checkRequiredConfigProperty("outputSlot", outputSlot, issues);

  }
  @Override
  protected void invokeInternal(WorkflowContext ctx, ProgressMonitor m, Issues issues) {
    Collection<?> slotContent = (Collection<?>) ctx.get(modelSlot);
    if (slotContent==null) {
      issues.addError(String.format("Slot %s is empty", modelSlot));
      return;
    }
    List<Entity> entities = Lists.newArrayList();
    for (Resource r : Iterables.filter(slotContent, XbaseResource.class)) {
      for (Entity e : EcoreUtil2.eAllOfType(r.getContents().get(0), Entity.class)) {
        entities.add(e);
      }
    }
    if (entities.isEmpty()) {
      issues.addError("No Entity instance found in model slot");
      return;
    }

    // execute the transformation
    Database transformed = new Domainmodel2Dbmodel().transform(entities);
    ctx.set(outputSlot, transformed);
  }
}
