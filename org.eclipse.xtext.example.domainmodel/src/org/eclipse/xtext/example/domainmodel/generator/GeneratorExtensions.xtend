package org.eclipse.xtext.example.domainmodel.generator

import org.eclipse.xtext.example.domainmodel.*
import org.eclipse.xtext.example.domainmodel.domainmodel.*
import org.eclipse.emf.ecore.*
import org.eclipse.xtext.common.types.*
import java.util.Set
import org.eclipse.xtext.xbase.compiler.*

class GeneratorExtensions extends DomainmodelExtensions {

  def shortName(JvmTypeReference r, ImportManager importManager) {
    val builder = new StringBuilder()
    importManager.appendTypeRef(r, builder)
    builder.toString
  }

  def fileName(Entity e) {
  e.packageName.folderName + '/' + e.name + '.java'
  }

  def folderName(String javaPackageName) {
  if(javaPackageName != null) javaPackageName.replace('.', '/') else ''
  }

  def parameterList(Operation o, ImportManager importManager) {
    o.params.map(p| p.parameterType.shortName(importManager) + ' ' + p.name).join(''',
      '''
    )
  }

  def isInterface(JvmTypeReference typeRef) {
    (typeRef.type as JvmGenericType).isInterface
  }

  def xmlFileName(Entity e) {
  e.packageName.folderName + "/" + e.name + '.xml'
  }
}