package org.eclipse.xtext.example.domainmodel.validation;

import java.util.Set;

import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.example.domainmodel.domainmodel.DomainmodelPackage;
import org.eclipse.xtext.example.domainmodel.domainmodel.Entity;
import org.eclipse.xtext.example.domainmodel.domainmodel.Property;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.ValidationMessageAcceptor;

import com.google.common.collect.Sets;
import com.google.inject.Inject;

public class DomainmodelJavaValidator extends AbstractDomainmodelJavaValidator {
  @Inject
  private IQualifiedNameProvider qualifiedNameProvider;

  @Check
  public void checkTypeNameStartsWithCapital(Entity entity) {
    if (!Character.isUpperCase(entity.getName().charAt(0))) {
      warning("Name should start with a capital",
          DomainmodelPackage.Literals.ENTITY__NAME,
          ValidationMessageAcceptor.INSIGNIFICANT_INDEX,
          IssueCodes.INVALID_TYPE_NAME,
          entity.getName());
    }
  }

  @Check
  public void checkFeatureNameStartsWithLowercase(Property feature) {
    if (!Character.isLowerCase(feature.getName().charAt(0))) {
      warning("Name should start with a lowercase",
          DomainmodelPackage.Literals.FEATURE__NAME,
          ValidationMessageAcceptor.INSIGNIFICANT_INDEX,
          IssueCodes.INVALID_FEATURE_NAME,
          feature.getName());
    }
  }

  /**
   * Validation: Entities must not have circular inheritances.
   */
  @Check
  public void validateInheritanceHierarchy(Entity ctx) {
    Set<QualifiedName> visited = Sets.newHashSet();
    visited.add(qualifiedNameProvider.getFullyQualifiedName(ctx));
    JvmGenericType current = null;
    if (ctx.getSuperType()!=null)
      current = (JvmGenericType) ctx.getSuperType().getType();

    while (current != null) {
        QualifiedName qfn = qualifiedNameProvider.getFullyQualifiedName(current);
        if (visited.contains(qfn)) {
          error("Circular inheritance detected",
              DomainmodelPackage.Literals.ENTITY__SUPER_TYPE);
          break;
        }
        // remember the visited Entity
        visited.add(qfn);
        // Set current to next in hierarchy. Might be null, which terminates
        // the loop
        if (!current.getSuperTypes().isEmpty())
          current = (JvmGenericType) current.getSuperTypes().get(0).getType();
        else
          current = null;
    }
  }

}