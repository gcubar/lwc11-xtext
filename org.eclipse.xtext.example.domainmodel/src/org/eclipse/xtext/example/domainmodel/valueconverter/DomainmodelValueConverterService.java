package org.eclipse.xtext.example.domainmodel.valueconverter;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.xbase.conversion.XbaseValueConverterService;

import com.google.inject.Singleton;

/**
 * Adds a value conversion for the QualifiedNameWithWildCard rule.
 */
@SuppressWarnings("restriction")
@Singleton
public class DomainmodelValueConverterService extends XbaseValueConverterService {

  @ValueConverter(rule = "QualifiedNameWithWildCard")
  public IValueConverter<String> getQualifiedNameWithWildCard() {
    return getQualifiedNameValueConverter();
  }
}
