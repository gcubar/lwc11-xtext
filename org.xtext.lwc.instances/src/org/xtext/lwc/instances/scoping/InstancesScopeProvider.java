package org.xtext.lwc.instances.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.MapBasedScope;
import org.eclipse.xtext.xbase.XBlockExpression;
import org.eclipse.xtext.xbase.XVariableDeclaration;
import org.eclipse.xtext.xbase.scoping.XbaseScopeProvider;
import org.xtext.lwc.instances.instances.Instance;

import com.google.common.collect.Iterables;
import com.google.inject.internal.Lists;

@SuppressWarnings("all")
public class InstancesScopeProvider extends XbaseScopeProvider {

  @Override
  public IScope createLocalVarScopeForBlock(final XBlockExpression block, final int indexOfContextExpressionInBlock,
      final IScope parentScope) {
    IEObjectDescription objDesc = EObjectDescription.create(QualifiedName.create("this"), block);
    return MapBasedScope.createScope(parentScope, Lists.newArrayList(objDesc));
  }

  public IScope createLocalVarScope(final EObject context, final EReference reference, final IScope parentScope,
      final boolean includeCurrentBlock, final int idx) {
    if (context instanceof Instance) {
      Iterable<XVariableDeclaration> localVars = Iterables.filter(((Instance) context).getExpressions(), XVariableDeclaration.class);
      return MapBasedScope.createScope(parentScope, Scopes.scopedElementsFor(localVars));
    } else {
      return super.createLocalVarScope(context, reference, parentScope, includeCurrentBlock, idx);
    }
  }
}